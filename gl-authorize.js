
var program=require('commander');

var gitlab=require('./gitlab-utils');
program
	.arguments('<repo> <user>')
	.option('-n, --namespace <namespace>','the group the repo belongs to')
	.option('-t, --token <token>','app token for gitlab')
	.action(function(repo,user) {
			gitlab.token=program.token;
            var request_data={}
            var project_path=repo;
            if(program.namespace)
                project_path=program.namespace+'%2F'+repo;
            request_data.id=project_path;
            request_data.user_id=gitlab.get_user_id(user);
            request_data.access_level="30";
            console.log(gitlab.submitRequest('POST','projects/'+project_path+'/members',request_data));
		}
	).parse(process.argv);
