var XMLHttpRequest=require("xmlhttprequest").XMLHttpRequest;

var gitlabUtils=function (){
    var self=this;
	self.submitRequest= function(type, url, request_data)
	{
		var baseUrl='https://gitlab.com/api/v3/';
        console.log('Type:',type);
        console.log(url);
        console.log(request_data);

		var xmlHttp = new XMLHttpRequest();
        console.log(baseUrl+url);
		xmlHttp.open( type, baseUrl+url, false ); // false for synchronous request
		xmlHttp.setRequestHeader('PRIVATE-TOKEN',self.token);
		xmlHttp.setRequestHeader('Content-Type','application/json');
        xmlHttp.send( JSON.stringify(request_data) );
        console.log(xmlHttp.responseText);
		return xmlHttp.responseText;
	}
	self.get_user_id=function (user) {
		var ans=self.submitRequest('GET', 'users?username='+user, null);
        return JSON.parse(ans)[0].id;
	}

	self.get_namespace_id=function (namespace,token) {
		var ans=self.submitRequest('GET', 'groups/'+namespace, null);
		return JSON.parse(ans).id;
	}
    
    return self;
}();

module.exports=gitlabUtils;