#!/usr/local/bin/node
var program=require('commander');

program
	.version('0.0.1')
	.command('createrepo <repo>', 'Create a gitlab repo')
	.command('deleterepo <repo>', 'delete a gitlab repo')
	.command('authorize <repo> <user>', 'Authorize a user to access a repo')
	.parse(process.argv);