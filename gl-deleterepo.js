var program=require('commander');
var XMLHttpRequest=require("xmlhttprequest").XMLHttpRequest;

function submitRequest(type, url, request_data, token)
{
	var baseUrl='https://gitlab.com/api/v3/';
    var xmlHttp = new XMLHttpRequest();
    console.log(baseUrl+url);
    xmlHttp.open( type, baseUrl+url, false ); // false for synchronous request
	xmlHttp.setRequestHeader('PRIVATE-TOKEN',token);
	xmlHttp.setRequestHeader('Content-Type','application/json');
    xmlHttp.send( request_data );
    return xmlHttp.responseText;
}

program
	.arguments('<repo>')
	.option('-n, --namespace <namespace>','the group/namespace the repo will belong to')
	.option('-t, --token <token>','app token for gitlab')
	.action(function(repo) {
            var path=repo;
            if(program.namespace) {
                // %2F is url encoding for /
                path=program.namespace + '%2F' + repo; 
            }
            var url='projects/'+path;
			console.log(submitRequest('DELETE',url,null,program.token));
		}
	).parse(process.argv);
