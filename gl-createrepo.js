
var program=require('commander');
var XMLHttpRequest=require("xmlhttprequest").XMLHttpRequest;

var gitlab = function() {
	var self=this;
	self.submitRequest= function(type, url, request_data)
	{
		var baseUrl='https://gitlab.com/api/v3/';

		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open( type, baseUrl+url, false ); // false for synchronous request
		xmlHttp.setRequestHeader('PRIVATE-TOKEN',self.token);
		xmlHttp.setRequestHeader('Content-Type','application/json');
		xmlHttp.send( request_data );
		return xmlHttp.responseText;
	}
	self.get_namespace_id=function (namespace,token) {
		var ans=self.submitRequest('GET', 'groups/'+namespace, null);
		return JSON.parse(ans).id;
	}

	return self;
}()


function get_user_id(user, token) {

}


program
	.arguments('<repo>')
	.option('-n, --namespace <namespace>','the group the repo will belong to')
	.option('-t, --token <token>','app token for gitlab')
	.action(function(repo) {
			gitlab.token=program.token;
			var req_data={};
			req_data.name=repo;
			if(program.namespace)
				req_data.namespace_id=gitlab.get_namespace_id(program.namespace, program.token)
			console.log(JSON.stringify(req_data));
			console.log(gitlab.submitRequest('POST','projects',JSON.stringify(req_data),program.token));
		}
	).parse(process.argv);
